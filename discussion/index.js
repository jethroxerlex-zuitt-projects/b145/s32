const express = require('express');
const mongoose = require('mongoose')
const app = express();

const port = 4000;

//Middlewares
//connecting to MongoDB
mongoose.connect("mongodb+srv://jramos:aketch24@wdc028-course-booking.il1nt.mongodb.net/session32?retryWrites=true&w=majority",
{
	useNewUrlParser : true,
	useUnifiedTopology: true
});
let db = mongoose.connection;
db.on("error", console.error.bind(console, "There is an error with the connection"))
db.once("open",() => console.log("Successfully connected to the database"));

app.use(express.json())
app.use(express.urlencoded({extended:true}))

// //Mongoose Schemas
// 	//schema === blueprint of your data

// 	const taskSchema = new mongoose.Schema({
	const userSchema = new mongoose.Schema({
		email: String,
		username: String,
		password: String,
		age: Number,
		isAdmin: {
			type: Boolean,
			default: false
		}

	});
// 		name: String,
// 		status: {
// 			type: String,
// 			default: 'pending'
// 		}
// 	});

// //Model

// const Task = mongoose.model("Task", taskSchema);
   const User = mongoose.model("User", userSchema);

// // Business Logic

// //Creating a new task
// app.post("/tasks",(req,res) => {

// 	Task.findOne({name: req.body.name},(err,result) => {

// 		if (result != null && result.name === req.body.name) {
// 			return res.send(`Duplicate task found: ${err}`)
// 		}else {
// 			let newTask = new Task({
// 				name: req.body.name
// 			});
// 			newTask.save((saveErr,savedTask) => {
// 				if (saveErr) {
// 					return console.error(saveErr)
// 				}else {
// 					return res.status(200).send(`New task created : ${savedTask}`)
// 				};
// 			});
// 		};
// 	});
// });	
// //RETRIEVE ALL TASK
// app.get("/tasks",(req,res) => {

// 	Task.find({},(err,result) => {
// 		if (err) {
// 			return console.log(err);
// 		}else {
// 			return res.status(200).json({
// 				tasks :result
// 			})
// 		}
// 	})
// })

// //Updating Task Name
// app.put("/tasks/update/:taskId",(req,res) => {

// 	let taskId = req.params.taskId
// 	let name = req.body.name

// 	Task.findByIdAndUpdate(taskId, {name:name},(err,updatedTask) => {
// 		if (err) {
// 			console.log(err)
// 		} else{
// 			res.send(`Congratulations the task has been updated`);
// 		}
// 	})
// })

// //DELETE TASK 
// app.delete("/tasks/archive-task/:taskId",(req,res) => {

// 	let taskId = req.params.taskId;

// 	Task.findByIdAndDelete(taskId,(err,deletedTask) => {
// 		if (err) {
// 			console.log(err)
// 		} else {
// 			res.send(`${deletedTask} has been deleted`)
// 		}
// 	})
// })


		//ACTIVITY

//CREATE A NEW USER
app.post("/users/signup",(req,res) => {
		User.findOne({email: req.body.email},(err,result) => {

		if (result != null && result.email === req.body.email) {
			return res.send(`Duplicate User found: ${err}`)
		}else {
			let newUser = new User({
				email: req.body.email,
				username: req.body.username,
				password: req.body.password,
				age: req.body.age 
			});
			newUser.save((saveErr,savedUser) => {
				if (saveErr) {
					return console.error(saveErr)
				}else {
					return res.status(200).send(`New User created : ${savedUser}`)
				};
			});
		};
	});
});	
	
	//RETRIEVE ALL USERS			
app.get("/users",(req,res) => {

	User.find({},(err,result) => {
		if (err) {
			return console.log(err);
		}else {
			return res.status(200).json({
				users :result
			})
		}
	})
})


	// // //Updating User Name
app.put("/users/update-user/:userId",(req,res) => {

	let userId = req.params.userId
	let username = req.body.username

	User.findByIdAndUpdate(userId, {username:username},(err,updatedTask) => {
		if (err) {
			console.log(err)
		} else{
			res.send(`Congratulations your username has been updated`);
		}
	})
})

	//DELETE USER
app.delete("/users/archive-users/:userId",(req,res) => {

	let userId = req.params.userId;

	User.findByIdAndDelete(userId,(err,deletedUser) => {
		if (err) {
			console.log(err)
		} else {
			res.send(`${deletedUser} has been deleted`)
		}
	})
})	
	
app.listen(port,()=> console.log(`Server running at port ${port}`))